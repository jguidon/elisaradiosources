package org.radios.elisa.export;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.neovisionaries.i18n.CountryCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.radios.elisa.stationimport.Station;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;
import java.util.Collection;

import static org.radios.elisa.export.ElisaRadioExport.ElisaRadio.anElisaRadio;
import static org.radios.elisa.export.ElisaRadioExport.anElisaRadioExport;


@Component
@RequiredArgsConstructor
public class Exporter {
    private static String frenchExportLocation = "src/main/resources/export/frenchStations.json";

    public void exportByCountry(@NonNull Collection<Station> stations, @NonNull CountryCode countryCode) {
        write(anElisaRadioExport()
                .radios(stations.stream()
                        .filter(station -> countryCode == station.getCountryCode())
                        .map(station -> anElisaRadio()
                                .id(station.getStationId())
                                .name(station.getName())
                                .streamUrl(station.getUrlResolved())
                                .imageAddress(station.getFavicon())
                                .comment(station.getHomePage())
                                .build())
                        .toList())
                .build());
    }

    @SneakyThrows
    private void write(@NonNull Object object) {
        objectMapper().writeValue(new File(frenchExportLocation), object);
    }

    private ObjectMapper objectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule());
    }
}
