package org.radios.elisa.export;

import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.UUID;

@Value
@Builder
public class ElisaRadioExport {

    List<ElisaRadio> radios;

    public static ElisaRadioExportBuilder anElisaRadioExport() {
        return new ElisaRadioExportBuilder();
    }

    @Builder
    record ElisaRadio(UUID id, String name, String streamUrl, String comment, String imageAddress) {
        public static ElisaRadioBuilder anElisaRadio() {
            return new ElisaRadioBuilder();
        }
    }
}
