package org.radios.elisa;

import com.neovisionaries.i18n.CountryCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.radios.elisa.export.Exporter;
import org.radios.elisa.stationimport.Parser;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.URL;

@SpringBootApplication
@RequiredArgsConstructor
@Log4j2
public class ElisaApplication implements CommandLineRunner {

    private final Parser parser;

    private final Exporter exporter;

    public static void main(String[] args) {
        SpringApplication.run(ElisaApplication.class, args);
    }

	@Override
	public void run(String... args) throws Exception {
        var stations = parser.from(new URL("file:src/main/resources/stations.json"));

        exporter.exportByCountry(stations, CountryCode.FR);
	}
}
