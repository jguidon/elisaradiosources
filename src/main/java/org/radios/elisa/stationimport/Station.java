package org.radios.elisa.stationimport;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.neovisionaries.i18n.CountryCode;
import lombok.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
@Builder
@AllArgsConstructor(access = PRIVATE)
@Setter(PRIVATE)
@Getter
@EqualsAndHashCode
public class Station {

    @JsonProperty("changeuuid")
    UUID changeId;

    @JsonProperty("stationuuid")
    UUID stationId;

    String name;

    String url;

    @JsonProperty("url_resolved")
    String urlResolved;

    @JsonProperty("homepage")
    String homePage;

    String favicon;

    String tags;

    @JsonDeserialize(using = CustomCountryCodeDeserializer.class)
    @JsonProperty("countrycode")
    CountryCode countryCode;

    String state;

    String language;

    @JsonProperty("languagecodes")
    String languageCodes;

    int votes;

    @JsonProperty("lastchangetime_iso8601")
    LocalDateTime lastchangetime; //: 2022-11-01 08:40:32,

    String codec;

    int bitrate;

    @JsonDeserialize(using = CustomBooleanDeserializer.class)
    Boolean hls;//boolean

    @JsonDeserialize(using = CustomBooleanDeserializer.class)
    @JsonProperty("lastcheckok")
    Boolean lastCheckOk;

    @JsonProperty("lastchecktime_iso8601")
    LocalDateTime lastCheckTime;

    @JsonProperty("lastlocalchecktime_iso8601")
    LocalDateTime lastCheckOkTime;

    @JsonProperty("ssl_error")
    Boolean sslError;

    @JsonProperty("geo_lat")
    Double geoLat;

    @JsonProperty("geo_long")
    Double geoLong;

    /**
     * For testing purpose only
     */
    private static StationBuilder aStation() {
        return new StationBuilder();
    }

    static class CustomBooleanDeserializer extends StdDeserializer<Boolean> {

        protected CustomBooleanDeserializer() {
            this(null);
        }

        protected CustomBooleanDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public Boolean deserialize(
                JsonParser jsonparser, DeserializationContext context)
                throws IOException {
            return jsonparser.getIntValue() == 1;
        }
    }

    static class CustomCountryCodeDeserializer extends StdDeserializer<CountryCode> {

        protected CustomCountryCodeDeserializer() {
            this(null);
        }

        protected CustomCountryCodeDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public CountryCode deserialize(
                JsonParser jsonparser, DeserializationContext context)
                throws IOException {
            if ("".equals(jsonparser.getValueAsString())) {
                return null;
            }

            return CountryCode.getByAlpha2Code(jsonparser.getValueAsString());
        }
    }

}
