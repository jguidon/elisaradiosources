package org.radios.elisa.stationimport

import com.neovisionaries.i18n.CountryCode
import spock.lang.Specification

import java.time.LocalDateTime

import static org.radios.elisa.stationimport.Station.aStation

class ParserSpec extends Specification {

    Parser parser

    List<Station> stationsFromTestResource = [
            aStation()
                    .changeId(UUID.fromString("610cafba-71d8-40fc-bf68-1456ec973b9d"))
                    .stationId(UUID.fromString("941ef6f1-0699-4821-95b1-2b678e3ff62e"))
                    .name("Best FM")
                    .url("http://stream.bestfm.sk/128.mp3")
                    .urlResolved("http://stream.bestfm.sk/128.mp3")
                    .homePage("http://bestfm.sk/")
                    .favicon("")
                    .tags("")
                    .countryCode(CountryCode.SK)
                    .state("")
                    .language("")
                    .languageCodes("")
                    .votes(11)
                    .lastchangetime(LocalDateTime.of(2022, 11, 1, 8, 40, 32))
                    .codec("MP3")
                    .bitrate(128)
                    .hls(false)
                    .lastCheckOk(true)
                    .lastCheckTime(LocalDateTime.of(2023, 01, 28, 15, 26, 46))
                    .lastCheckOkTime(LocalDateTime.of(2023, 01, 28, 15, 26, 46))
                    .sslError(false)
                    .geoLat(null)
                    .geoLong(null)
                    .build(),
            aStation()
                    .changeId(UUID.fromString("7602eb7b-2486-43f7-a236-6390289700d4"))
                    .stationId(UUID.fromString("9d69cc77-b698-40c0-8036-17cd1f09ca44"))
                    .name("Fun Radio")
                    .url("http://stream.funradio.sk:8000/fun128.mp3")
                    .urlResolved("http://stream.funradio.sk:8000/fun128.mp3")
                    .homePage("http://www.funradio.sk/")
                    .favicon("http://cdn.funradio.sk/img/logo/apple-icon-120x120.png?v=2022-05-26-1422")
                    .tags("")
                    .countryCode(CountryCode.SK)
                    .state("")
                    .language("")
                    .languageCodes("")
                    .votes(18)
                    .lastchangetime(LocalDateTime.of(2022, 11, 1, 8, 9, 41))
                    .codec("MP3")
                    .bitrate(128)
                    .hls(false)
                    .lastCheckOk(true)
                    .lastCheckTime(LocalDateTime.of(2023, 01, 28, 14, 37, 55))
                    .lastCheckOkTime(LocalDateTime.of(2023, 01, 28, 14, 37, 55))
                    .sslError(false)
                    .geoLat(null)
                    .geoLong(null)
                    .build()

    ]

    def setup() {
        parser = new Parser()
    }

    def "Stations are correctly parsed"() {
        when:
        def result = parser.from(new URL("file:src/test/resources/stations.json"))

        then:
        result.size() == 2
        result.get(0) == stationsFromTestResource.get(0)
        result.get(1) == stationsFromTestResource.get(1)
    }
}
