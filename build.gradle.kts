plugins {
	java
	groovy
	id("org.springframework.boot") version "3.0.2"
	id("io.spring.dependency-management") version "1.1.0"
}

group = "org.radios"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.springframework.boot:spring-boot-starter-json:3.0.2")
	implementation(platform("org.apache.groovy:groovy-bom:4.0.5"))
	implementation("org.apache.groovy:groovy")
	implementation("com.neovisionaries:nv-i18n:1.29")
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")

	testImplementation("org.springframework.boot:spring-boot-starter-test")

//	implementation ("org.codehaus.groovy:groovy:3.0.5")
//	testImplementation("org.spockframework:spock-core:2.0-M3-groovy-3.0")
//	testImplementation("org.spockframework:spock-spring:2.0-M3-groovy-3.0")
//	testImplementation("org.springframework.boot:spring-boot-starter-test") {
//		exclude group: "org.junit.vintage", module: "junit-vintage-engine"
//	}
	testImplementation(platform("org.spockframework:spock-bom:2.3-groovy-4.0"))
	testImplementation ("org.spockframework:spock-core")
//
	testRuntimeOnly("net.bytebuddy:byte-buddy:1.12.17")
	testRuntimeOnly("org.objenesis:objenesis:3.3")
//	testRuntimeOnly("com.h2database:h2:2.1.214")
}

tasks.withType<Test> {
	useJUnitPlatform()
//	testLogging (
//		events "passed", "skipped", "failed"
//	)
}
